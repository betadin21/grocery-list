import React from "react";
import { Component } from "react";

import Btn from "../Btn";
import Prod from "../Prod"

import "./style.css";

import { clickAdd } from "../../methods";

class App extends Component {
  state = {
    itemArr: [],
    inputValue : "",
  };
  clickSave = () => {
    // записую value в масив
    let inputValue = document.querySelector(".input").value;
    if(inputValue !== ""){
      this.state.itemArr.push(inputValue);
    }
    

    // обнуляю value на сторінці
    this.setState((state)=>{
      return{
        ...state,
        inputValue : ""
      }
    })
  }

  setValue = (e) => {
    this.setState((state)=>{
      return{
        ...state,
        inputValue : e
      }
    })
  }

 
  render() {
    return (
      <>
        <h1>Grocery List</h1>
        <div className="wrapper">
          <Btn value={"Add ✚"} click={clickAdd} />
          <div className="input-container">
            <input
              value={this.state.inputValue}
              onChange={(e)=>{this.setValue(e.target.value)}}
              className="input"
              type="text"
              placeholder="text the product, please"
            />
            <Btn value={"Save 📀"} click={this.clickSave} />
          </div>
          <div className="list">
            {this.state.itemArr.map((el,i)=> (<Prod arr ={this.state.itemArr} text={el} key={i} id={i}/>)) }
          </div>
        </div>
      </>
    );
  }
}
export default App;
